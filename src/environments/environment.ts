// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBqttbULWVgXxi6LL4K-Xs9UBKrmEmYuHM',
    authDomain: 'typescript-platzi-4abfb.firebaseapp.com',
    databaseURL: 'https://typescript-platzi-4abfb.firebaseio.com',
    projectId: 'typescript-platzi-4abfb',
    storageBucket: 'typescript-platzi-4abfb.appspot.com',
    messagingSenderId: '981137711624',
    appId: '1:981137711624:web:a424dfb809974128'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
