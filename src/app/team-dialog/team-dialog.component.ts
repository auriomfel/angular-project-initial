import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Countries } from '../interfaces/player';
import { take } from 'rxjs/operators';
import { TeamService } from '../services/team.service';
import { NgForm } from '@angular/forms';
import { Team } from '../interfaces/team';

@Component({
  selector: 'app-team-dialog',
  templateUrl: './team-dialog.component.html',
  styleUrls: ['./team-dialog.component.scss']
})
export class TeamDialogComponent implements OnInit {
  @Input() team: Team;
  @Output() closeDialog: EventEmitter<boolean> = new EventEmitter();
  public nationality = Object.keys(Countries).map(key => ({
    label: key, key: Countries[key]
  }));
  constructor(private teamService: TeamService) {

  }

  ngOnInit() {

  }
  private newTeam(teamFormValue) {
    const key = this.teamService.addTeam(teamFormValue).key;
    const teamFormValueKey = {
      ...teamFormValue,
      key
    }
  }
  private editTeam(teamFormValue) {
    const teamFormValueWithKey = {
      ...teamFormValue,
      $key: this.team.$key
    }
    this.teamService.editTeam(teamFormValueWithKey);
  }
  onSubmit(teamForm: NgForm) {
    const teamFormValue: Team = { ...teamForm.value };
    if (teamForm.valid) {
      if (this.team) {
        this.editTeam(teamFormValue)
      } else {
        this.newTeam(teamFormValue)
      }
      this.closeDialog.emit(true)

    }
  }
  onClose() {
    this.closeDialog.emit(true)
  }
}