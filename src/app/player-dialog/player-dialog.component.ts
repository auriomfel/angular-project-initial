import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Countries, SquadNumber, Player } from '../interfaces/player';
import { PlayerService } from '../services/player.service';
import { take } from 'rxjs/operators';
import { TeamService } from '../services/team.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-player-dialog',
  templateUrl: './player-dialog.component.html',
  styleUrls: ['./player-dialog.component.scss']
})
export class PlayerDialogComponent implements OnInit {
  @Input() player: Player;
  @Output() closeDialog: EventEmitter<boolean> = new EventEmitter();
  private team;
  public countries = Object.keys(Countries).map(key => ({
    label: key, key: Countries[key]
  }))
  public squadNumber = Object.keys(SquadNumber).slice(Object.keys(SquadNumber).length / 2).map(key => ({
    label: key, key: SquadNumber[key]
  }))
  constructor(private playerService: PlayerService, private teamService: TeamService) { }

  ngOnInit() {
    this.teamService
      .getTeams()
      .pipe(take(1))
      .subscribe(teams => {
        if (teams.length > 0) {
          this.team = teams[0];
        }
      });
  }
  private newPlayer(playerFormValue) {
    const key = this.playerService.addPlayer(playerFormValue).key;
    const playerFormValueKey = {
      ...playerFormValue,
      key
    }

    if (this.team.country === playerFormValueKey.nationality) {
      const formatedTeam = {
        ...this.team,
        players: [
          ...(this.team.players ? this.team.players : []),
          playerFormValueKey
        ]
      }
      this.teamService.editTeam(formatedTeam);
    }


  }

  private editPlayer(playerFormValue) {
    const playerFormValueWithKey = {
      ...playerFormValue,
      $key: this.player.$key
    }
    const playerFormValueWithFormattedKey = {
      ...playerFormValue,
      key: this.player.$key
    }
    delete playerFormValueWithFormattedKey.$key;
    const moddifiedPlayers = this.team.players ?
      this.team.players.map(player => {
        return player.key === this.player.$key ? playerFormValueWithFormattedKey : player;
      }) : this.team.players;

    this.playerService.editPlayer(playerFormValueWithKey);
    let formattedTeam = {}
    if (this.team.country === this.player.nationality) {
      if (playerFormValue.nationality !== this.team.country) {
        formattedTeam = {
          ...this.team,
          players: [...(moddifiedPlayers ? moddifiedPlayers.filter(player => player.key !== playerFormValueWithFormattedKey.key) : [])]
        };
      }
      else {
        formattedTeam = {
          ...this.team,
          players: [...(moddifiedPlayers ? moddifiedPlayers : [playerFormValueWithFormattedKey])]
        }
      }
    } else {
      if (playerFormValue.nationality === this.team.country) {
        let newPlayers = moddifiedPlayers.concat([playerFormValueWithFormattedKey]);
        console.log(newPlayers);
        formattedTeam = {
          ...this.team,
          players: [...(moddifiedPlayers ? moddifiedPlayers.concat([playerFormValueWithFormattedKey]) : moddifiedPlayers)]
        };
      }
      else {
        formattedTeam = {
          ...this.team,
          players: [...(moddifiedPlayers ? moddifiedPlayers : [playerFormValueWithFormattedKey])]
        }
      }
    }

    this.teamService.editTeam(formattedTeam);

  }

  onSubmit(playerForm: NgForm) {
    const playerFormValue = { ...playerForm.value };
    if (playerForm.valid) {
      playerFormValue.leftFooted = playerFormValue.leftFooted === '' ? false : playerFormValue.leftFooted;
      if (this.player) {
        this.editPlayer(playerFormValue)
      } else {
        this.newPlayer(playerFormValue);
      }
      this.closeDialog.emit(true)
    }


    window.location.replace('#');

  }
  onClose() {
    this.closeDialog.emit(true)
  }
}
