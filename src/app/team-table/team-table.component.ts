import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { take, subscribeOn } from 'rxjs/operators';
import { Team } from '../interfaces/team';
import { Player } from '../interfaces/player';
import { Countries } from '../interfaces/player';
import { PlayerService } from '../services/player.service';
import { TeamTableHeaders, TeamService } from '../services/team.service';

@Component({
  selector: 'app-team-table',
  templateUrl: './team-table.component.html',
  styleUrls: ['./team-table.component.scss']
})
export class TeamTableComponent implements OnInit {
  public teams$: Observable<Team[]>;
  public tableHeaders = TeamTableHeaders;
  public selectedTeam: Team;
  public showModal = false;
  constructor(private teamService: TeamService, private playerService: PlayerService) { }

  ngOnInit() {
    this.teams$ = this.teamService.getTeams();
    this.teamService
      .getTeams()
      .pipe(take(1))
      .subscribe(teams => {
        if (teams.length === 0) {
          const team: Team = {
            name: 'My Amazing Team',
            country: Countries.Peru,
            players: null
          };
          this.teamService.addTeam(team)
        }
      });
  }
  newTeam() {
    this.showModal = true;
    this.selectedTeam = null;
    setTimeout(() => {
      window.location.replace('#open-modal');
    });
  }
  editTeam(team: Team) {
    this.selectedTeam = { ...team };
    this.showModal = true;
    setTimeout(() => {
      window.location.replace('#open-modal');
    });
  }
  deleteTeam(team: Team) {
    this.playerService
      .getPlayers()
      .pipe(take(1))
      .subscribe(players => {
        players.forEach(
          player => {
            player.nationality === team.country ? this.playerService.deletePlayer(player.$key):'';
          }
        )
      })
    this.teamService.deleteTeam(team.$key);
  }
  closeDialog() {
    this.showModal = false;
  }
}
